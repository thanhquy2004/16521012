#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + y, yc + x);
	SDL_RenderDrawPoint(ren, xc - y, yc + x);
	SDL_RenderDrawPoint(ren, xc + y, yc - x);
	SDL_RenderDrawPoint(ren, xc - y, yc - x);

}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = R, y = 0;
	int p = 3 - 2 * R;
	Draw8Points(xc, yc, x, y, ren);
	while (x > y) {
		if (p <= 0) {
			p = p + 4 * y + 6;
		}
		else {
			p = p + 4 * y - 4 * x + 10;
			x--;
		}
		y++;
		Draw8Points(xc, yc, x, y, ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = R;
	int y = 0;
	int p = 1 - R;
	Draw8Points(xc, yc, x, y, ren);
	while (x > y)
	{
		if (p <= 0) p = p + 2 * y + 3;
		else
		{
			p = p + 2 * (y - x) + 5;
			x = x - 1;
		}
		y = y + 1;
		Draw8Points(xc, yc, x, y, ren);
	}
}
