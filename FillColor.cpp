#include "FillColor.h"
#include<vector>
#include <iostream>
using namespace std;

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
	SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

	Uint32 temp;
	Uint8 red, green, blue, alpha;

	/* Get Red component */
	temp = pixel & fmt->Rmask;  /* Isolate red component */
	temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
	red = (Uint8)temp;

	/* Get Green component */
	temp = pixel & fmt->Gmask;  /* Isolate green component */
	temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
	green = (Uint8)temp;

	/* Get Blue component */
	temp = pixel & fmt->Bmask;  /* Isolate blue component */
	temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
	blue = (Uint8)temp;

	/* Get Alpha component */
	temp = pixel & fmt->Amask;  /* Isolate alpha component */
	temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
	temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
	alpha = (Uint8)temp;

	SDL_Color color = { red, green, blue, alpha };
	return color;

}

//Get all pixels on the window

SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);
	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				delete[] pixels;
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
			delete[] pixels;
		}
	}
	return saveSurface;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
	if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
		return true;
	return false;
}

void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{

}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	int flag = c;
	if (a >= b && a >= c)
		 flag = a;
	if  (b >= a && b >= c)
		flag = b;
	return flag;
}

int minIn3(int a, int b, int c)
{
	int flag = a;
	if (b <= a && b <= c)
		flag = b;
	if (c <= a && c <= b)
		flag = c;
	return flag;
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D temp;
	b = temp;
	b = a;
	a = temp;
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
	{
		if (v1.y > v2.y) 
			swap(v1, v2);
		if (v1.y > v3.y)
			swap(v1, v3);
		if (v2.y > v3.y)
			swap(v2, v3);
	}

	void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
	{
		int Min = minIn3(v1.x, v2.x, v3.x);
		int Max = maxIn3(v1.x, v2.x, v3.x);
		Bresenham_Line(Min, v1.y, Max, v1.y, ren);
	}

	void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
	{
		float a = (float)(v3.x - v1.x) / (v3.y - v1.y);
		float b = (float)(v3.x - v2.x) / (v3.y - v2.y);
		float X_LEFT = v1.x;
		float X_RIGHT = v2.x;
		for (int i = v1.y; i <= v3.y; i++)
		{
			X_LEFT += a;
			X_RIGHT += b;
			Bresenham_Line(int(X_LEFT + 0.5), i, int(X_RIGHT + 0.5), i, ren);
		}
	}

	void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
	{
		float a = (float)(v2.x - v1.x) / (v2.y - v1.y);
		float b = (float)(v3.x - v1.x) / (v3.y - v1.y);
		float X_LEFT = v1.x;
		float X_RIGHT = v1.x;
		for (int i = v1.y; i <= v3.y; i++)
		{
			X_LEFT += a;
			X_RIGHT += b;
			Bresenham_Line(int(X_LEFT + 0.5), i, int(X_RIGHT + 0.5), i, ren);
		}
	}

	void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
	{
		float b = (float)(v3.x - v1.x) / (v3.y - v1.y);
		float a = (float)(v2.x - v1.x) / (v2.y - v1.y);
		float X_LEFT = v1.x;
		float X_RIGHT = v1.x;
		for (int i = v1.y; i <= v3.y; i++)
		{
			X_LEFT += a;
			X_RIGHT += b;
			Bresenham_Line(int(X_LEFT + 0.5), i, int(X_RIGHT + 0.5), i, ren);
		}
	}

	void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
	{
		ascendingSort(v1, v2, v3);
		SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
		if (v1.y == v2.y && v2.y == v3.y) {
			TriangleFill1(v1, v2, v3, ren, fillColor);
			return;
		}
		if (v1.y == v2.y && v2.y < v3.y) {
			TriangleFill2(v1, v2, v3, ren, fillColor);
			return;
		}
		if (v1.y < v2.y && v3.y == v2.y) {
			TriangleFill3(v1, v2, v3, ren, fillColor);
			return;
		}
		if (v1.y < v2.y && v2.y < v3.y) {
			TriangleFill4(v1, v2, v3, ren, fillColor);
			return;
		}
		return;
	}

